import 'dart:convert';

import 'package:fsel_youtube_app/models/youtube_item_model.dart';
import 'package:fsel_youtube_app/models/youtube_model.dart';
import 'package:fsel_youtube_app/utils/app_constants.dart';
import 'package:http/http.dart' as http;

class AppServices {
  AppServices();

  Future<YouTubeModel?> getInfo() async {
    try {
      final res = await http.get(Uri.parse(
        AppConstants.BASE_URL +
            '/videos?part=snippet,contentDetails,statistics&chart=mostPopular&maxResults=3&key=' +
            AppConstants.API_KEY,
      ));

      if (res.statusCode == 200) {
        final data = json.decode(res.body);
        print('success here');

        return YouTubeModel.fromJson(data);
      } else {
        return null;
      }
    } catch (e) {
      print('ERROR api ${e.toString()}');
      return null;
    }
  }

  Future<YoutubeItemModel?> getItemInfo(String id) async {
    try {
      final res = await http.get(Uri.parse(
        AppConstants.BASE_URL +
            '/videos?part=snippet,contentDetails,statistics&id=' +
            id +
            '&key=' +
            AppConstants.API_KEY,
      ));

      if (res.statusCode == 200) {
        final data = json.decode(res.body);
        print('success here');

        return YoutubeItemModel.fromJson(data);
      } else {
        return null;
      }
    } catch (e) {
      print('ERROR api ${e.toString()}');
      return null;
    }
  }
}
