import 'package:fsel_youtube_app/models/snippet_model.dart';
import 'package:fsel_youtube_app/models/statistics_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'item_model.g.dart';

@JsonSerializable()
class Items {
  final String kind;
  final String etag;
  final String id;
  final Snippet snippet;
  final Statistics statistics;

  Items({
    required this.kind,
    required this.etag,
    required this.id,
    required this.snippet,
    required this.statistics,
  });

  factory Items.fromJson(Map<String, dynamic> json) => _$ItemsFromJson(json);

  Map<String, dynamic> toJson() => _$ItemsToJson(this);
}
