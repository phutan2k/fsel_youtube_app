import 'package:fsel_youtube_app/models/item_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'youtube_item_model.g.dart';

@JsonSerializable()
class YoutubeItemModel {
  final String kind;
  final String etag;
  final List<Items> items;

  YoutubeItemModel({
    required this.kind,
    required this.etag,
    required this.items,
  });

  factory YoutubeItemModel.fromJson(Map<String, dynamic> json) =>
      _$YoutubeItemModelFromJson(json);

  Map<String, dynamic> toJson() => _$YoutubeItemModelToJson(this);
}
