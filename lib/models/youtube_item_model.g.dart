// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'youtube_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

YoutubeItemModel _$YoutubeItemModelFromJson(Map<String, dynamic> json) =>
    YoutubeItemModel(
      kind: json['kind'] as String,
      etag: json['etag'] as String,
      items: (json['items'] as List<dynamic>)
          .map((e) => Items.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$YoutubeItemModelToJson(YoutubeItemModel instance) =>
    <String, dynamic>{
      'kind': instance.kind,
      'etag': instance.etag,
      'items': instance.items,
    };
