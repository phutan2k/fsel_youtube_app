// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'thumbnails_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Thumbnails _$ThumbnailsFromJson(Map<String, dynamic> json) => Thumbnails(
      standard: Default.fromJson(json['standard'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ThumbnailsToJson(Thumbnails instance) =>
    <String, dynamic>{
      'standard': instance.standard,
    };
