import 'package:fsel_youtube_app/models/default_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'thumbnails_model.g.dart';

@JsonSerializable()
class Thumbnails {
  final Default standard;

  Thumbnails({required this.standard});

  factory Thumbnails.fromJson(Map<String, dynamic> json) =>
      _$ThumbnailsFromJson(json);

  Map<String, dynamic> toJson() => _$ThumbnailsToJson(this);
}