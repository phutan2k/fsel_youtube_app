import 'package:fsel_youtube_app/models/item_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'youtube_model.g.dart';

@JsonSerializable()
class YouTubeModel {
  final String kind;
  final String etag;
  final List<Items> items;

  YouTubeModel({
    required this.kind,
    required this.etag,
    required this.items,
  });

  factory YouTubeModel.fromJson(Map<String, dynamic> json) =>
      _$YouTubeModelFromJson(json);

  Map<String, dynamic> toJson() => _$YouTubeModelToJson(this);
}
