// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment_model.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$CommentModelCWProxy {
  CommentModel comments(List<String> comments);

  CommentModel id(String id);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `CommentModel(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// CommentModel(...).copyWith(id: 12, name: "My name")
  /// ````
  CommentModel call({
    List<String>? comments,
    String? id,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfCommentModel.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfCommentModel.copyWith.fieldName(...)`
class _$CommentModelCWProxyImpl implements _$CommentModelCWProxy {
  final CommentModel _value;

  const _$CommentModelCWProxyImpl(this._value);

  @override
  CommentModel comments(List<String> comments) => this(comments: comments);

  @override
  CommentModel id(String id) => this(id: id);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `CommentModel(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// CommentModel(...).copyWith(id: 12, name: "My name")
  /// ````
  CommentModel call({
    Object? comments = const $CopyWithPlaceholder(),
    Object? id = const $CopyWithPlaceholder(),
  }) {
    return CommentModel(
      comments: comments == const $CopyWithPlaceholder() || comments == null
          ? _value.comments
          // ignore: cast_nullable_to_non_nullable
          : comments as List<String>,
      id: id == const $CopyWithPlaceholder() || id == null
          ? _value.id
          // ignore: cast_nullable_to_non_nullable
          : id as String,
    );
  }
}

extension $CommentModelCopyWith on CommentModel {
  /// Returns a callable class that can be used as follows: `instanceOfCommentModel.copyWith(...)` or like so:`instanceOfCommentModel.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$CommentModelCWProxy get copyWith => _$CommentModelCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentModel _$CommentModelFromJson(Map<String, dynamic> json) => CommentModel(
      id: json['id'] as String? ?? '',
      comments: (json['comments'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
    );

Map<String, dynamic> _$CommentModelToJson(CommentModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'comments': instance.comments,
    };
