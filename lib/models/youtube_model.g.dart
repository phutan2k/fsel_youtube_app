// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'youtube_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

YouTubeModel _$YouTubeModelFromJson(Map<String, dynamic> json) => YouTubeModel(
      kind: json['kind'] as String,
      etag: json['etag'] as String,
      items: (json['items'] as List<dynamic>)
          .map((e) => Items.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$YouTubeModelToJson(YouTubeModel instance) =>
    <String, dynamic>{
      'kind': instance.kind,
      'etag': instance.etag,
      'items': instance.items,
    };
