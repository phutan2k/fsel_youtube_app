import 'package:json_annotation/json_annotation.dart';

part 'default_model.g.dart';

@JsonSerializable()
class Default {
  final String url;
  final int width;
  final int height;

  Default({required this.url,required this.width,required this.height});

  factory Default.fromJson(Map<String, dynamic> json) =>
      _$DefaultFromJson(json);

  Map<String, dynamic> toJson() => _$DefaultToJson(this);
}