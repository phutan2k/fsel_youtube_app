import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_youtube_app/blocs/youtube_blocs.dart';
import 'package:fsel_youtube_app/blocs/youtube_events.dart';
import 'package:fsel_youtube_app/blocs/youtube_states.dart';
import 'package:fsel_youtube_app/widgets/build_text_field_widget.dart';
import 'package:fsel_youtube_app/widgets/reusable_icon_widget.dart';
import 'package:fsel_youtube_app/widgets/reusable_text_widget.dart';

class CommentPage extends StatefulWidget {
  const CommentPage({
    Key? key,
    required this.id,
    required this.index,
  }) : super(key: key);
  final String id;
  final int index;

  @override
  State<CommentPage> createState() => _CommentPageState();
}

class _CommentPageState extends State<CommentPage> {
  TextEditingController commentController = TextEditingController();

  @override
  void initState() {
    super.initState();
    BlocProvider.of<YoutubeBloc>(context).add(CommentEvent(widget.id));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.pink[50],
      child: SafeArea(
        child: Scaffold(
          body: BlocBuilder<YoutubeBloc, YoutubeStates>(
            builder: (context, state) {
              return state.item != null
                  ? Container(
                      color: Colors.pink[50],
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              color: Colors.pink[50],
                              margin: EdgeInsets.only(bottom: 10),
                              height: 65,
                              child: Row(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: ReusableIconWidget(
                                      iconData: Icons.arrow_back_ios_new,
                                    ),
                                  ),
                                  ReusableTextWidget(
                                    text: state
                                        .item!.items[0].snippet.channelTitle,
                                    size: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 20, right: 20),
                              child: ReusableTextWidget(
                                text: 'Description:',
                                size: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                left: 20,
                                right: 20,
                                top: 5,
                              ),
                              child: ReusableTextWidget(
                                text: state.item!.items[0].snippet.description,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                left: 10,
                                right: 10,
                              ),
                              child: Row(
                                children: [
                                  ReusableIconWidget(
                                    iconData: Icons.comment_rounded,
                                    iconSize: 20,
                                  ),
                                  Expanded(
                                    child: BuildTextFieldWidget(
                                      controller: commentController,
                                      hintText: 'Writes your comment!',
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () => onTapped(),
                                    child: ReusableIconWidget(
                                      iconData: Icons.arrow_forward,
                                      iconSize: 20,
                                      iconColor: Colors.white,
                                      backgroundColor:
                                          Colors.pink.withOpacity(0.6),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              height: 500,
                              margin: EdgeInsets.only(
                                left: 20,
                                right: 20,
                              ),
                              child: ListView.builder(
                                itemCount: state.comments.length,
                                itemBuilder: (_, index) {
                                  return Row(
                                    children: [
                                      Expanded(child: SizedBox()),
                                      Container(
                                        margin:
                                        EdgeInsets.only(bottom: 5),
                                        padding: EdgeInsets.symmetric(
                                          vertical: 10,
                                          horizontal: 10,
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                          BorderRadius.circular(20),
                                          color: Colors.pink[300],
                                        ),
                                        child: ReusableTextWidget(
                                          text: state.comments[index]!,
                                          color: Colors.white,
                                          size: 16,
                                        ),
                                      ),
                                    ],
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : Center(
                      child: CircularProgressIndicator(
                        color: Colors.red,
                      ),
                    );
            },
          ),
        ),
      ),
    );
  }

  void onTapped() {
    if (commentController.value.text.trim().isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: ReusableTextWidget(
            text: 'Comment is empty',
            color: Colors.white,
          ),
          duration: Duration(
            seconds: 2,
          ),
          backgroundColor: Colors.pink.withOpacity(0.6),
        ),
      );
    } else {
      BlocProvider.of<YoutubeBloc>(context).add(
        CommentValueEvent(
          commentController.value.text,
        ),
      );
    }
  }
}
