import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_youtube_app/blocs/youtube_blocs.dart';
import 'package:fsel_youtube_app/blocs/youtube_events.dart';
import 'package:fsel_youtube_app/blocs/youtube_states.dart';
import 'package:fsel_youtube_app/models/youtube_model.dart';
import 'package:fsel_youtube_app/pages/comment_page/comment_page.dart';
import 'package:fsel_youtube_app/widgets/reusable_icon_widget.dart';
import 'package:fsel_youtube_app/widgets/reusable_text_widget.dart';
import 'package:share_plus/share_plus.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<YoutubeBloc>(context).add(InitYoutubeEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.pink[50],
      child: SafeArea(
        child: Scaffold(
          body: BlocBuilder<YoutubeBloc, YoutubeStates>(
            builder: (context, state) {
              if (state.model != null) {
                YouTubeModel youTubeModel = state.model!;
                return Container(
                  color: Colors.pink[50],
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                    top: 20,
                  ),
                  child: ListView.builder(
                    itemCount: youTubeModel.items.length,
                    itemBuilder: (_, index) {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        height: 380,
                        margin: EdgeInsets.only(bottom: 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 200,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                image: DecorationImage(
                                  image: NetworkImage(
                                    youTubeModel.items[index].snippet.thumbnails
                                        .standard.url,
                                  ),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                left: 10,
                                right: 10,
                                top: 10,
                              ),
                              child: ReusableTextWidget(
                                text: youTubeModel.items[index].snippet.title,
                                fontWeight: FontWeight.bold,
                                size: 16,
                              ),
                            ),
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 10,
                                      left: 10,
                                      right: 10,
                                    ),
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      image: DecorationImage(
                                        image: NetworkImage(
                                          youTubeModel.items[index].snippet
                                              .thumbnails.standard.url,
                                        ),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  ReusableTextWidget(
                                    text: youTubeModel
                                        .items[index].snippet.channelTitle,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      String json =
                                          youTubeModel.toJson().toString();
                                      Share.share(json);
                                    },
                                    child: Container(
                                      padding: EdgeInsets.only(right: 10),
                                      margin: EdgeInsets.only(left: 20),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey[100],
                                      ),
                                      child: Row(
                                        children: [
                                          ReusableIconWidget(
                                            iconData: Icons.share_outlined,
                                          ),
                                          ReusableTextWidget(
                                            text: 'Share',
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 10,
                                right: 5,
                              ),
                              child: Row(
                                children: [
                                  ReusableIconWidget(
                                    iconData: Icons.favorite,
                                  ),
                                  ReusableTextWidget(
                                    text: youTubeModel
                                        .items[index].statistics.likeCount,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  ReusableIconWidget(
                                    iconData: Icons.remove_red_eye_sharp,
                                  ),
                                  ReusableTextWidget(
                                    text: youTubeModel
                                        .items[index].statistics.viewCount,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) => CommentPage(
                                            id: youTubeModel.items[index].id,
                                            index: index,
                                          ),
                                        ),
                                      );
                                    },
                                    child: Container(
                                      padding: EdgeInsets.only(right: 10),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey[100],
                                      ),
                                      child: Row(
                                        children: [
                                          ReusableIconWidget(
                                            iconData: Icons.comment_rounded,
                                          ),
                                          ReusableTextWidget(
                                            text: 'Comments',
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                );
              }
              return Center(
                child: CircularProgressIndicator(
                  color: Colors.red,
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
