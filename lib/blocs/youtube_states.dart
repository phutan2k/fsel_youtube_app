import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:fsel_youtube_app/models/comment_model.dart';
import 'package:fsel_youtube_app/models/youtube_item_model.dart';
import 'package:fsel_youtube_app/models/youtube_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'youtube_states.g.dart';

@JsonSerializable()
@CopyWith()
class YoutubeStates extends Equatable {
  final YouTubeModel? model;
  final YoutubeItemModel? item;
  final List<String?> comments;

  YoutubeStates({
    this.model,
    this.item,
    this.comments = const [],
  });

  @override
  List<Object?> get props => [
        model,
        item,
        comments,
      ];

  factory YoutubeStates.fromJson(Map<String, dynamic> json) =>
      _$YoutubeStatesFromJson(json);

  Map<String, dynamic> toJson() => _$YoutubeStatesToJson(this);
}
