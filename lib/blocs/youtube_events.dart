import 'package:equatable/equatable.dart';
import 'package:fsel_youtube_app/models/comment_model.dart';

sealed class YoutubeEvents extends Equatable {
  YoutubeEvents();

  @override
  List<Object?> get props => [];
}

class InitYoutubeEvent extends YoutubeEvents {
  InitYoutubeEvent();

  @override
  List<Object?> get props => [];
}

class CommentEvent extends YoutubeEvents {
  final String id;

  CommentEvent(this.id);

  @override
  List<Object?> get props => [];
}

class CommentValueEvent extends YoutubeEvents {
  final String textValue;

  CommentValueEvent(this.textValue);

  @override
  List<Object?> get props => [];
}
