// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'youtube_states.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$YoutubeStatesCWProxy {
  YoutubeStates comments(List<String?> comments);

  YoutubeStates item(YoutubeItemModel? item);

  YoutubeStates model(YouTubeModel? model);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `YoutubeStates(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// YoutubeStates(...).copyWith(id: 12, name: "My name")
  /// ````
  YoutubeStates call({
    List<String?>? comments,
    YoutubeItemModel? item,
    YouTubeModel? model,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfYoutubeStates.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfYoutubeStates.copyWith.fieldName(...)`
class _$YoutubeStatesCWProxyImpl implements _$YoutubeStatesCWProxy {
  final YoutubeStates _value;

  const _$YoutubeStatesCWProxyImpl(this._value);

  @override
  YoutubeStates comments(List<String?> comments) => this(comments: comments);

  @override
  YoutubeStates item(YoutubeItemModel? item) => this(item: item);

  @override
  YoutubeStates model(YouTubeModel? model) => this(model: model);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `YoutubeStates(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// YoutubeStates(...).copyWith(id: 12, name: "My name")
  /// ````
  YoutubeStates call({
    Object? comments = const $CopyWithPlaceholder(),
    Object? item = const $CopyWithPlaceholder(),
    Object? model = const $CopyWithPlaceholder(),
  }) {
    return YoutubeStates(
      comments: comments == const $CopyWithPlaceholder() || comments == null
          ? _value.comments
          // ignore: cast_nullable_to_non_nullable
          : comments as List<String?>,
      item: item == const $CopyWithPlaceholder()
          ? _value.item
          // ignore: cast_nullable_to_non_nullable
          : item as YoutubeItemModel?,
      model: model == const $CopyWithPlaceholder()
          ? _value.model
          // ignore: cast_nullable_to_non_nullable
          : model as YouTubeModel?,
    );
  }
}

extension $YoutubeStatesCopyWith on YoutubeStates {
  /// Returns a callable class that can be used as follows: `instanceOfYoutubeStates.copyWith(...)` or like so:`instanceOfYoutubeStates.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$YoutubeStatesCWProxy get copyWith => _$YoutubeStatesCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

YoutubeStates _$YoutubeStatesFromJson(Map<String, dynamic> json) =>
    YoutubeStates(
      model: json['model'] == null
          ? null
          : YouTubeModel.fromJson(json['model'] as Map<String, dynamic>),
      item: json['item'] == null
          ? null
          : YoutubeItemModel.fromJson(json['item'] as Map<String, dynamic>),
      comments: (json['comments'] as List<dynamic>?)
              ?.map((e) => e as String?)
              .toList() ??
          const [],
    );

Map<String, dynamic> _$YoutubeStatesToJson(YoutubeStates instance) =>
    <String, dynamic>{
      'model': instance.model,
      'item': instance.item,
      'comments': instance.comments,
    };
