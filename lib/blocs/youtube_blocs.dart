import 'dart:async';

import 'package:fsel_youtube_app/blocs/youtube_events.dart';
import 'package:fsel_youtube_app/blocs/youtube_states.dart';
import 'package:fsel_youtube_app/models/comment_model.dart';
import 'package:fsel_youtube_app/services/app_services.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

class YoutubeBloc extends HydratedBloc<YoutubeEvents, YoutubeStates> {
  YoutubeBloc() : super(YoutubeStates()) {
    on<InitYoutubeEvent>(onInitYoutubeEvent);
    on<CommentEvent>(onCommentEvent);
    on<CommentValueEvent>(onCommentValueEvent);
  }

  AppServices appServices = AppServices();

  @override
  YoutubeStates? fromJson(Map<String, dynamic> json) {
    return YoutubeStates.fromJson(json);
  }

  @override
  Map<String, dynamic>? toJson(YoutubeStates state) {
    return state.toJson();
  }

  FutureOr<void> onInitYoutubeEvent(
    InitYoutubeEvent event,
    Emitter<YoutubeStates> emit,
  ) async {
    try {
      final data = await appServices.getInfo();

      if (data != null) {
        emit(state.copyWith(model: data));
      }
    } catch (e) {
      print('ERROR: ${e.toString()}');
    }
  }

  FutureOr<void> onCommentEvent(
    CommentEvent event,
    Emitter<YoutubeStates> emit,
  ) async {
    try {
      final data = await appServices.getItemInfo(event.id);

      if (data != null) {
        emit(state.copyWith(item: data));
      }
    } catch (e) {
      print('ERROR: ${e.toString()}');
    }
  }

  FutureOr<void> onCommentValueEvent(
    CommentValueEvent event,
    Emitter<YoutubeStates> emit,
  ) {
    final comments = [...state.comments];
    comments.add(event.textValue);
    emit(state.copyWith(comments: comments));
  }
}
